#include "Animation.h"

#include "SDL2Common.h"

//for printf
#include <stdio.h>
#include <stdexcept>

/**
 * initAnimation
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param anim Animation structure to populate 
 * @param noOfFrames Frames of animation
 * @param SPRITE_WIDTH Width of the sprite
 * @param SPRITE_HEIGHT Height of the sprite
 * @param row Row of the grid to take the sprites from or -1 if working along a row. 
 * @param col Column of the grid to take the sprites from or -1 if along a column.
 * @return void?  Could do with returning a value for success/error 
 */

Animation::Animation()
{
    // Should be set by init - use frames
    // to check for init not being called.
    maxFrames = 0;
    frames = nullptr;
    // Need to be able to set this!
    frameTimeMax = 0.4f; // Default to 2.5fps
    //set current animation frame to first frame.
    currentFrame = 0;
    //zero frame time accumulator
    accumulator = 0.0f;
}

/**
 * destAnimation
 * 
 * Function to clean up an animation structure.  
 * 
 * @param anim Animation structure to destroy
 */
Animation::~Animation()
{
    //Free the memory - we allocated it with new
    // so must use the matching delete operation.
    delete[] frames;
    // Good practice to set pointers to null after deleting
    // ths prevents accidental access.
    frames = nullptr;
}

void Animation::init(int noOfFrames,
    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col)
{
    // set frame count.
    maxFrames = noOfFrames;
    // allocate frame array
    frames = new SDL_Rect[maxFrames];
    //Setup animation frames - fixed row!
    for (int i = 0; i < maxFrames; i++)
    {
        if (row == -1)
        {
            frames[i].x = (i * SPRITE_WIDTH); //ith col.
            frames[i].y = (col * SPRITE_HEIGHT); //col row.
        }
        else
        {
            if (col == -1)
            {
                frames[i].x = (row * SPRITE_WIDTH); //ith col.
                frames[i].y = (i * SPRITE_HEIGHT); //col row.
            }
            else
            {
                // Throwing errors is more C++ than return values.
                throw std::runtime_error("Bad parameters to init");
            }
        }
        frames[i].w = SPRITE_WIDTH;
        frames[i].h = SPRITE_HEIGHT;
    }
}

SDL_Rect* Animation::getCurrentFrame()
{
    return &frames[currentFrame];
}

void Animation::update(float timeDeltaInSeconds)
{
    // Add elapsed time to the animation accumulator.
    accumulator += timeDeltaInSeconds;
    // Check if animation needs update
    if (accumulator > frameTimeMax)
    {
        currentFrame++;
        accumulator = 0.0f;
        if (currentFrame >= maxFrames)
        {
            currentFrame = 0;
        }
    }
}
