#ifndef ANIMATION_H_
#define ANIMATION_H_

#include "SDL2Common.h"

class Animation
{
private:
    int maxFrames;
    int currentFrame;

    float frameTimeMax;
    float accumulator;

    SDL_Rect* frames;

public:
    Animation();
    ~Animation();

    void init(int noOfFrames,
        const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
        int row, int col);

    SDL_Rect* getCurrentFrame();

    void update(float deltaTime);
};

#endif