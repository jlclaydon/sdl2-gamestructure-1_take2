#include "Player.h"
#include "TextureUtils.h"

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param player Player structure to populate 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @return void?  Could do with returning a value for success/error 
 */
void initPlayer(Player* player, SDL_Renderer* renderer)
{
    // Create player texture from file, optimised for renderer
    player->texture = createTextureFromFile("assets/images/undeadking.png", renderer);
    // Allocate memory for the animation structures
    for (int i = 0; i < player->MAX_ANIMATIONS; i++)
    {
        player->animations[i] = new Animation();
    }
    // Setup the animation structure
    player->animations[LEFT]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 1);
    player->animations[RIGHT]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 2);
    player->animations[UP]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 3);
    player->animations[DOWN]->init(3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0);
    player->animations[IDLE]->init(1, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 0); // set the default state to idle
        player->state = IDLE;
    // Target is the same size of the source
    player->targetRectangle.w = player->SPRITE_WIDTH;
    player->targetRectangle.h = player->SPRITE_HEIGHT;
}

/**
 * destPlayer
 * 
 * Function to clean up an player structure.  
 * 
 * @param player Player structure to destroy
 */
void destPlayer(Player* player)
{
    // Clean up animations - free memory
    for (int i = 0; i < player->MAX_ANIMATIONS; i++)
    {
        // Clean up the animaton object
        // allocated with new so use delete.
        delete player->animations[i];
        player->animations[i] = nullptr;
    }
    // Clean up
    SDL_DestroyTexture(player->texture);
    player->texture = nullptr;
}

/**
 * drawPlayer
 * 
 * Function draw a Player structure
 * 
 * @param player Player structure tp draw
 * @param renderer SDL_Renderer to draw to
 */
void drawPlayer(Player* player, SDL_Renderer *renderer)
{
    // Get current animation (only have one)!
    Animation* current = player->animations[player->state];

    SDL_RenderCopy(renderer, player->texture, current->getCurrentFrame(), &player->targetRectangle);

}

/**
 * processInput
 * 
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 * 
 * @param player Player structure processing the input
 * @param keyStates The keystates array. 
 */
void processInput(Player *player, const Uint8 *keyStates)
{
    // Process Player Input
        
    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP]) 
    {
        verticalInput = -1.0f;
    }
    else 
    {
        verticalInput = 0.0f;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    player->vy = verticalInput * player->speed;
    player->vx = horizontalInput * player->speed;
}

/**
 * updatePlayer
 * 
 * Function to update the player structure and its components
 * 
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */
void updatePlayer(Player* player, float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * player->vy;
    float xMovement = timeDeltaInSeconds * player->vx;

    // Update player position.
    player->x += xMovement;
    player->y += yMovement;

    // Move sprite to nearest pixel location.
    player->targetRectangle.y = round(player->y);
    player->targetRectangle.x = round(player->x);

    // Get current animation
    Animation* current = player->animations[player->state];

    // let animation update itself.
    current->update(timeDeltaInSeconds);
}