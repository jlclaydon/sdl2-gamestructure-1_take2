#include "Player.h"
#include "TextureUtils.h"

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param player Player structure to populate 
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @return void?  Could do with returning a value for success/error 
 */
void initPlayer(Player* player, SDL_Renderer *renderer)
{

    // Create player texture from file, optimised for renderer 
    // There are better ways of doing this, i.e. a Resource Manager 
    // This will do for now :-D
    player->texture = createTextureFromFile("assets/images/undeadking.png", renderer);

    // Allocate memory for the animation structure
    player->walkLeft = new Animation;

    // Setup the animation structure
    initAnimation(player->walkLeft, 3, player->SPRITE_WIDTH, player->SPRITE_HEIGHT, -1, 2);

    // Target is the same size of the source
    // We could choose any size - experiment with this :-D
    player->targetRectangle.w = player->SPRITE_WIDTH;
    player->targetRectangle.h = player->SPRITE_HEIGHT;
}

/**
 * destPlayer
 * 
 * Function to clean up an player structure.  
 * 
 * @param player Player structure to destroy
 */
void destPlayer(Player* player)
{
    // Clean up animations - free memory
    destAnimation(player->walkLeft);

    // Clean up the animaton structure
    // allocated with new so use delete. 
    delete player->walkLeft;
    player->walkLeft = nullptr;

    // Clean up 
    SDL_DestroyTexture(player->texture);
    player->texture = nullptr; 
}

/**
 * drawPlayer
 * 
 * Function draw a Player structure
 * 
 * @param player Player structure tp draw
 * @param renderer SDL_Renderer to draw to
 */
void drawPlayer(Player* player, SDL_Renderer *renderer)
{
    // Get current animation (only have one)!
    Animation* current = player->walkLeft;

    SDL_RenderCopy(renderer, player->texture, &current->frames[current->currentFrame], &player->targetRectangle);
}

/**
 * processInput
 * 
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 * 
 * @param player Player structure processing the input
 * @param keyStates The keystates array. 
 */
void processInput(Player *player, const Uint8 *keyStates)
{
    // Process Player Input
        
    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP]) 
    {
        verticalInput = -1.0f;
    }
    else 
    {
        verticalInput = 0.0f;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    player->vy = verticalInput * player->speed;
    player->vx = horizontalInput * player->speed;
}

/**
 * updatePlayer
 * 
 * Function to update the player structure and its components
 * 
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */
void updatePlayer(Player* player, float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * player->vy;
    float xMovement = timeDeltaInSeconds * player->vx;

    // Update player position.
    player->x += xMovement;
    player->y += yMovement;

    // Move sprite to nearest pixel location.
    player->targetRectangle.y = round(player->y);
    player->targetRectangle.x = round(player->x);

    // Store animation time delta
    player->walkLeft->accumulator += timeDeltaInSeconds;

    // Check if animation needs update
    if(player->walkLeft->accumulator > 0.4f) //25fps?
    {
        player->walkLeft->currentFrame++;
        player->walkLeft->accumulator = 0.0f;

        if(player->walkLeft->currentFrame >= player->walkLeft->maxFrames)
        {
            player->walkLeft->currentFrame = 0;
        }
    }
}